package com.yance.fim.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.util.ClassUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    private static Logger logger = LoggerFactory.getLogger(Utils.class);

    /**
     * 验证是否是一个合法的邮箱
     *
     * @param string
     * @return
     */
    public static boolean isEmail(String string) {
        if (string == null)
            return false;
        String regEx1 = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
        Pattern p;
        Matcher m;
        p = Pattern.compile(regEx1);
        m = p.matcher(string);
        return m.matches();
    }

    public static void ConsoleExecptionLog(Exception e) {
        StringBuilder message = new StringBuilder();
        for (StackTraceElement stackTraceElement : e.getStackTrace()) {
            message.append(System.lineSeparator()).append(stackTraceElement.toString());
        }
        logger.error("Exception: {}", e.getMessage());
        logger.error(message.toString());
    }

    public static void ConsoleExecptionLog(Throwable e) {
        StringBuilder message = new StringBuilder();
        for (StackTraceElement stackTraceElement : e.getStackTrace()) {
            message.append(System.lineSeparator()).append(stackTraceElement.toString());
        }
        logger.error("Exception: {}", e.getMessage());
        logger.error(message.toString());
    }

    /**
     * 获取当前类下的所有的类
     * @param cls
     * @return
     * @throws Exception
     */
    public static List<Class<?>> getClassPathAllClass(Class<?> cls) throws Exception {
        String packagePath = cls.getPackage().getName();
        logger.info("className: [{}] packagePath [{}]",cls,packagePath);
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);
        // 加载系统所有类资源
        Resource[] resources = resourcePatternResolver.getResources("classpath*:" + packagePath.replaceAll("[.]", "/") + "/**/*.class");
        List<Class<?>> list = new ArrayList<>();
        // 把每一个class文件找出来
        for (Resource r : resources) {
            MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(r);
            Class<?> clazz = ClassUtils.forName(metadataReader.getClassMetadata().getClassName(), null);
            list.add(clazz);
        }
        return list;
    }

}
