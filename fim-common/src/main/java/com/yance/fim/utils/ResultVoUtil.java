package com.yance.fim.utils;

/**
 * @author yangpeng
 */
public class ResultVoUtil {
    public static ResultVo success(Object object) {
        ResultVo resultVO = new ResultVo();
        resultVO.setData(object);
        resultVO.setCode(1000);
        resultVO.setMsg("成功");
        return resultVO;
    }


    public static ResultVo<Void> success() {
        return success(null);
    }

    public static ResultVo<Void> success(Integer code, String msg) {
        ResultVo<Void> resultVO = new ResultVo<>();
        resultVO.setCode(code);
        resultVO.setMsg(msg);
        return resultVO;
    }


    public static ResultVo<Void> error(Integer code, String msg) {
        ResultVo<Void> resultVO = new ResultVo<>();
        resultVO.setCode(code);
        resultVO.setMsg(msg);
        return resultVO;
    }
}
