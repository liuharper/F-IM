package com.yance.fim.utils;


import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @param <T>
 * @author yangpeng
 */
public class ResultVo<T> {
    /**
     * 状态码.
     */
    private Integer code;

    /**
     * 提示信息.
     */
    private String msg;

    /**
     * 具体内容.
     * <p>
     * 属性为 空（""） 或者为 NULL 都不序列化
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private T data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
