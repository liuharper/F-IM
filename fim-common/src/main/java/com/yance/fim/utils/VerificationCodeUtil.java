package com.yance.fim.utils;

import java.util.Random;

/**
 * @author yance
 * 验证码字符串生成
 */
public class VerificationCodeUtil {

    //去除0 1 I L等易混淆的字母数字
    public static final String SEEDS = "23456789ABCDEFGHJKMNPQRSTUVWXYZ";
    public static final int NORMAL_SIZE = 4;
    private static Random random = new Random();

    public static String getVerificationCode() {
        return getVerificationCode(NORMAL_SIZE);
    }

    public static String getVerificationCode(int verificationSize) {
        return getVerificationCode(verificationSize, SEEDS);
    }

    public static String getVerificationCode(int verificationSize, String seeds) {
        seeds = seedsIsNullOrEmpty(seeds);
        int maxIndex = seeds.length();
        StringBuilder verificationCode = new StringBuilder(verificationSize);
        for (int i = 0; i < verificationSize; i++) {
            int randomIndex = random.nextInt(maxIndex);
            char seed = seeds.charAt(randomIndex);
            verificationCode.append(seed);
        }
        return verificationCode.toString();
    }

    public static String seedsIsNullOrEmpty(String seeds) {
        if (seeds == null || "".equals(seeds)) {
            seeds = SEEDS;
        }
        return seeds;
    }
}
