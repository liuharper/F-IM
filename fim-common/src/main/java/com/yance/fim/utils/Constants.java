package com.yance.fim.utils;

/**
 * 常量类
 *
 * @author yance
 */
public class Constants {

    public static final String ATTR_USERACCOUNT = "userAccount";

    public static final String ATTR_CLIENTID = "clientId";

    public static final String CHARSET = "utf-8";

    public static int CORE_POOL_SIZE = Runtime.getRuntime().availableProcessors() * 2;

    public static int MAX_POOL_SIZE = Runtime.getRuntime().availableProcessors() * 6;

    /**
     * 子处理器核心线程数
     */
    public static int SUB_PROCESSOR_CORE_POOL_SIZE = Runtime.getRuntime().availableProcessors();

    /**
     * 子处理器最大线程数
     */
    public static int SUB_PROCESSOR_MAX_POOL_SIZE = Runtime.getRuntime().availableProcessors() * 2;

    /**
     * 心跳包数据
     */
    public static String HEARTBEAT_PACKET_JSON_STRING = "{\"command\":100000}";
}
