package com.yance.fim.dto;

import java.io.Serializable;

public class WsMessageRequest implements Serializable {
    /**
     * 指令
     */
    private int command;

    /**
     * 消息体
     */
    private Body body;

    private class Body implements Serializable{
        /**
         * 客户端消息ID
         */
        private String clientMessageId;

        /**
         * 消息类型 1:文本、2:表情、3:图片、4:视频、5:文件、6:名片
         */
        private int messageType;

        /**
         * 消息来源
         */
        private String comeFrom;

        /**
         * 消息目标
         */
        private String target;

        /**
         * 目标类型  1:单聊、2:群聊
         */
        private int targetType;

        /**
         * 消息具体内容
         */
        private String content;

        public String getClientMessageId() {
            return clientMessageId;
        }

        public void setClientMessageId(String clientMessageId) {
            this.clientMessageId = clientMessageId;
        }

        public int getMessageType() {
            return messageType;
        }

        public void setMessageType(int messageType) {
            this.messageType = messageType;
        }

        public String getComeFrom() {
            return comeFrom;
        }

        public void setComeFrom(String comeFrom) {
            this.comeFrom = comeFrom;
        }

        public String getTarget() {
            return target;
        }

        public void setTarget(String target) {
            this.target = target;
        }

        public int getTargetType() {
            return targetType;
        }

        public void setTargetType(int targetType) {
            this.targetType = targetType;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public int getCommand() {
        return command;
    }

    public void setCommand(int command) {
        this.command = command;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }
}
