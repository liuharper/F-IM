package com.yance.fim.utils;

/**
 * 命令指令集
 */
public class CommandValue {
    /**
     * 心跳包指定
     */
    public static final int HEARTBEAT_VALUE = 1000;

    /**
     * 建立连接
     */
    public static final int CONNECTION_HANDLER_VALUE = 1001;

    /**
     * 主动断开连接
     */
    public static final int DISCONNECTION_HANDLER_VALUE = 1002;
}
