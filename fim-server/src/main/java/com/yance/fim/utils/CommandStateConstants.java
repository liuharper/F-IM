package com.yance.fim.utils;

/**
 * IM状态指令集
 */
public class CommandStateConstants {
    /**
     * 数据包解析异常
     */
    public static final Integer DATAPACKET_ANALYSIS_EXCEPTION = 4001;

    /**
     * 分发器处理异常
     */
    public static final Integer MESSAGE_DISPATCHER_EXCEPTION = 4002;

    /**
     * 分发器处理异常
     */
    public static final Integer MESSAGE_PROTOCOL_ERROR = 4003;
}
