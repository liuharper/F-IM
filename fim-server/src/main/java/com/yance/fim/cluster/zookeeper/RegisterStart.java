package com.yance.fim.cluster.zookeeper;

import com.yance.fim.config.ConfigParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.net.InetAddress;

/**
 * 注册启动器
 */
@Component
@Order(value = 2)
public class RegisterStart implements CommandLineRunner {

    private static Logger logger = LoggerFactory.getLogger(RegistryZK.class);

    @Autowired
    private ConfigParam configParam;

    @Override
    public void run(String... args) throws Exception {
        logger.info("-----------------RegisterStart Start-up-----------------");
        //获得本机IP
        String localHostAddress = InetAddress.getLocalHost().getHostAddress();
        Thread thread = new Thread(new RegistryZK(localHostAddress, configParam.getPort()));
        thread.setName("registry-zk");
        thread.start() ;
    }
}
