package com.yance.fim.cluster.zookeeper;


import com.yance.fim.config.ConfigParam;
import com.yance.fim.utils.SpringBeanFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Function:
 *
 * @author crossoverJie
 *         Date: 2018/8/24 01:37
 * @since JDK 1.8
 */
public class RegistryZK implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(RegistryZK.class);

    private ZKit zKit;

    private ConfigParam configParam ;

    private String ip;

    private int serverPort;

    public RegistryZK(String localHostAddress, int serverPort) {
        this.ip = localHostAddress;
        this.serverPort = serverPort;
        zKit = SpringBeanFactory.getBean(ZKit.class) ;
        configParam = SpringBeanFactory.getBean(ConfigParam.class) ;
    }

    @Override
    public void run() {
        //创建父节点
        zKit.createRootNode();
        //是否要将自己注册到 ZK
        if (configParam.isZkSwitch()){
            String path = configParam.getZkRoot() + "/" + ip + ":" + serverPort;
            zKit.createNode(path);
            logger.info("Registry zookeeper success, msg=[{}]", path);
        }
    }
}