package com.yance.fim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.tio.websocket.starter.EnableTioWebSocketServer;

/**
 * @author yance
 */
@EnableTioWebSocketServer
@SpringBootApplication
public class FimServerApplication {
    public static void main(String[] args) {
        //指定项目中使用的日志框架
        System.setProperty("dubbo.application.logger", "log4j2");
        SpringApplication.run(FimServerApplication.class, args);
    }
}
