package com.yance.fim.websocket.handler;

import com.yance.fim.utils.CommandValue;
import com.yance.fim.websocket.packet.DataPacket;

@Command(CommandValue.CONNECTION_HANDLER_VALUE)
public class ConnectHandler extends DefaultRequestHandler {

    @Override
    public void processRequest(DataPacket dataPacket) {
        System.err.println("ConnectHandler");
    }
}
