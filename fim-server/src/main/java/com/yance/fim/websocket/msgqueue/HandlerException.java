package com.yance.fim.websocket.msgqueue;

import com.lmax.disruptor.ExceptionHandler;
import com.yance.fim.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 自定义异常处理器
 */
public class HandlerException implements ExceptionHandler {
    private Logger logger = LoggerFactory.getLogger(HandlerException.class);

    /**
     * @see com.lmax.disruptor.ExceptionHandler#handleEventException(java.lang.Throwable, long, java.lang.Object)
     * 运行过程中发生时的异常
     * @param ex
     * @param sequence
     * @param event
     */
    public void handleEventException(Throwable ex, long sequence, Object event) {
        logger.error("process data error sequence ==[{}] event==[{}] ,ex ==[{}]", sequence, event.toString(), ex.getMessage());
        Utils.ConsoleExecptionLog(ex);
    }

    /**
     * 启动时的异常
     * @see com.lmax.disruptor.ExceptionHandler#handleOnStartException(java.lang.Throwable)
     * @param ex
     */
    public void handleOnStartException(Throwable ex) {
        logger.error("start disruptor error ==[{}]!", ex.getMessage());
        Utils.ConsoleExecptionLog(ex);
    }

    /**
     * 关闭时的异常
     * @see com.lmax.disruptor.ExceptionHandler#handleOnShutdownException(java.lang.Throwable)
     * @param ex
     */
    public void handleOnShutdownException(Throwable ex) {
        logger.error("shutdown disruptor error ==[{}]!", ex.getMessage());
        Utils.ConsoleExecptionLog(ex);
    }

}
