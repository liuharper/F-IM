package com.yance.fim.websocket;

import org.springframework.stereotype.Service;
import org.tio.core.ChannelContext;
import org.tio.core.intf.GroupListener;

/**
 * @author yance
 */
@Service
public class FimGroupListener implements GroupListener {

    @Override
    public void onAfterBind(ChannelContext channelContext, String s) {

    }

    @Override
    public void onAfterUnbind(ChannelContext channelContext, String s) {

    }
}
