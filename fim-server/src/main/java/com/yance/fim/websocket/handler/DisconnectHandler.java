package com.yance.fim.websocket.handler;

import com.yance.fim.utils.CommandValue;
import com.yance.fim.websocket.packet.DataPacket;

@Command(CommandValue.DISCONNECTION_HANDLER_VALUE)
public class DisconnectHandler extends DefaultRequestHandler {

    @Override
    public void processRequest(DataPacket dataPacket) {
        System.err.println("DisconnectHandler");
    }
}
