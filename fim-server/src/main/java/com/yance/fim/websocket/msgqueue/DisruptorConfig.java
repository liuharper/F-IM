package com.yance.fim.websocket.msgqueue;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DisruptorConfig {

    /**
     * smsParamEventHandler
     *
     * @return SeriesDataEventHandler
     */
    @Bean
    public DataPacketEventHandler smsParamEventHandler() {
        return new DataPacketEventHandler();
    }
}
