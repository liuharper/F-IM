package com.yance.fim.websocket.handler;

import com.yance.fim.websocket.packet.DataPacket;

public abstract class DefaultRequestHandler<L,R> {

    /**
     * handle message
     */
    abstract void processRequest(DataPacket dataPacket);

    void replyAck(DataPacket dataPacket, R r){
        //TODO 回应Ack
    }
}
