package com.yance.fim.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.tio.core.ChannelContext;
import org.tio.core.intf.Packet;
import org.tio.websocket.server.WsServerAioListener;

/**
 * @author tanyaowu
 * 用户根据情况来完成该类的实现
 */
@Service
public class FimServerAioListener extends WsServerAioListener {
    private static Logger log = LoggerFactory.getLogger(FimServerAioListener.class);

    private FimServerAioListener() {
    }

    @Override
    public void onAfterConnected(ChannelContext channelContext, boolean isConnected, boolean isReconnect) throws Exception {
        super.onAfterConnected(channelContext, isConnected, isReconnect);
        if (log.isDebugEnabled()) {
            log.info("onAfterConnected client {}", channelContext.getClientNode());
        }
    }

    @Override
    public void onAfterSent(ChannelContext channelContext, Packet packet, boolean isSentSuccess) throws Exception {
        super.onAfterSent(channelContext, packet, isSentSuccess);
        if (log.isDebugEnabled()) {
            log.info("onAfterSent client:" + channelContext.getClientNode() + " bsId " + channelContext.getBsId() + " sendSuccess " + isSentSuccess);
        }
    }

    @Override
    public void onBeforeClose(ChannelContext channelContext, Throwable throwable, String remark, boolean isRemove) throws Exception {
        super.onBeforeClose(channelContext, throwable, remark, isRemove);
        if (log.isDebugEnabled()) {
            log.info("onBeforeClose {}", channelContext);
        }
    }

    @Override
    public void onAfterDecoded(ChannelContext channelContext, Packet packet, int packetSize) {

    }

    @Override
    public void onAfterReceivedBytes(ChannelContext channelContext, int receivedBytes) {

    }

    @Override
    public void onAfterHandled(ChannelContext channelContext, Packet packet, long cost) {
        log.info("onAfterHandled client {} message {}", channelContext.getClientNode(), packet.logstr());
    }
}
