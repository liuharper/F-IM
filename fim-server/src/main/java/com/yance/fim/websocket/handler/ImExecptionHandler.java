package com.yance.fim.websocket.handler;

import com.yance.fim.utils.Utils;
import org.tio.core.ChannelContext;
import org.tio.core.Tio;
import org.tio.websocket.common.WsResponse;
import rx.Observable;
import rx.Observer;

import java.util.concurrent.TimeUnit;

public class ImExecptionHandler {

    /**
     * IM异常关闭
     * @param channelContext
     * @param command 指令
     */
    public static void ImExecptionClose(ChannelContext channelContext,Integer command) {
        Observable.just(command).filter(s -> Tio.send(channelContext, WsResponse.fromText(String.valueOf(command), "utf-8"))).delay(500, TimeUnit.MILLISECONDS).subscribe(new Observer<Integer>() {
            @Override
            public void onCompleted() {
                Tio.close(channelContext, String.valueOf(command));
            }

            @Override
            public void onError(Throwable throwable) {
                Utils.ConsoleExecptionLog(throwable);
            }

            @Override
            public void onNext(Integer command) {
            }
        });
    }
}
