package com.yance.fim.websocket.msgqueue;

import com.lmax.disruptor.WorkHandler;
import com.yance.fim.websocket.handler.MessageDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataPacketEventHandler implements WorkHandler<DataPacketEvent> {

    private Logger logger = LoggerFactory.getLogger(DataPacketEventHandler.class);

    public void onEvent(DataPacketEvent event) {
        MessageDispatcher.getInstance().dispatcher(event.getValue());
    }
}
