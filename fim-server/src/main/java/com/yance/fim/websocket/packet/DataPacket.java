package com.yance.fim.websocket.packet;

import org.tio.core.ChannelContext;

/**
 * 数据包
 */
public class DataPacket {
    /**
     * 指令
     */
    private int command;

    /**
     * 真正的消息内容
     */
    private byte[] body;

    private ChannelContext channelContext;

    public int getCommand() {
        return command;
    }

    public void setCommand(int command) {
        this.command = command;
    }

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

    public ChannelContext getChannelContext() {
        return channelContext;
    }

    public void setChannelContext(ChannelContext channelContext) {
        this.channelContext = channelContext;
    }
}
