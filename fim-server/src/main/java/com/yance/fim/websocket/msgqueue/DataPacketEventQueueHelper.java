package com.yance.fim.websocket.msgqueue;

import com.lmax.disruptor.*;
import com.yance.fim.websocket.packet.DataPacket;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DataPacketEventQueueHelper extends BaseQueueHelper<DataPacket, DataPacketEvent, DataPacketEventHandler> implements InitializingBean {

    /**
     * 2的13幂次方,最接近10000
     */
    private static final int QUEUE_SIZE = 8192;

    @Autowired
    private List<DataPacketEventHandler> seriesDataEventHandler;

    @Override
    protected int getQueueSize() {
        return QUEUE_SIZE;
    }

    @Override
    protected EventFactory<DataPacketEvent> eventFactory() {
        return DataPacketEvent::new;
    }

    @Override
    protected WorkHandler[] getHandler() {
        int size = seriesDataEventHandler.size();
        DataPacketEventHandler[] paramEventHandlers = seriesDataEventHandler.toArray(new DataPacketEventHandler[size]);
        return paramEventHandlers;
    }

    @Override
    protected WaitStrategy getStrategy() {
        return new YieldingWaitStrategy();
    }

    @Override
    public void afterPropertiesSet() {
        this.init();
    }
}
