package com.yance.fim;

import com.yance.fim.dto.ImUserLoginDto;
import org.nustaq.serialization.FSTConfiguration;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class FstTest {

    private static FSTConfiguration conf = FSTConfiguration.createJsonConfiguration();

    public static void main(String[] args) throws UnsupportedEncodingException {
        ImUserLoginDto dto = new ImUserLoginDto();
        dto.setEmail("2444864349@qq.com");
        dto.setPassword("87uhkasdk");
        String s = conf.asJsonString("to jsonString:" + dto);
        System.out.println(s);
        byte[] bytes = conf.asByteArray(dto);
        System.out.println("byte[] to String : " + new String(bytes, StandardCharsets.UTF_8));
    }
}
