package com.yance.fim;

import com.yance.fim.utils.Utils;
import com.yance.fim.websocket.handler.Command;
import com.yance.fim.websocket.handler.MessageDispatcher;

import java.util.List;

public class ClassTest {
    public static void main(String[] args) throws Exception {
        List<Class<?>> classpath = Utils.getClassPathAllClass(MessageDispatcher.class);
        for (Class<?> aClass : classpath) {
            System.out.println(aClass.getSimpleName());
            Command annotation = aClass.getAnnotation(Command.class);
            if(annotation != null) {
                System.err.println("Command annotation:" + aClass.getSimpleName());
            }
        }
    }
}
