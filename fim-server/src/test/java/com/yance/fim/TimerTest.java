package com.yance.fim;

import java.util.Timer;
import java.util.TimerTask;

public class TimerTest {
    public static void main(String[] args) {
        Timer timer = new Timer();
        //每 3s 发送一次
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("我是一个定时执行的任务");
            }
        }, 3000, 3000);
    }

}
