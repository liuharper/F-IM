package com.yance.fim;

import cn.hutool.core.util.StrUtil;
import com.yance.fim.utils.Utils;
import org.tio.websocket.client.WebSocket;
import org.tio.websocket.client.WsClient;
import org.tio.websocket.client.config.WsClientConfig;
import org.tio.websocket.client.event.CloseEvent;
import org.tio.websocket.client.event.ErrorEvent;
import org.tio.websocket.client.event.MessageEvent;
import org.tio.websocket.client.event.OpenEvent;
import org.tio.websocket.common.WsPacket;

import java.util.function.Consumer;

public class WebsocketClientTest {
    public static void main(String[] args) {
        try {
            WsClient wsClient = WsClient.create("ws://127.0.0.1:9327", new WsClientConfig(new Consumer<OpenEvent>() {
                        @Override
                        public void accept(OpenEvent openEvent) {
                            System.out.println("emit open");
                        }
                    }, new Consumer<MessageEvent>() {
                        @Override
                        public void accept(MessageEvent messageEvent) {
                            WsPacket data = messageEvent.data;
                            String wsBodyText = data.getWsBodyText();
                            System.err.println("收到消息:" + wsBodyText);
                        }
                    }, new Consumer<CloseEvent>() {
                        @Override
                        public void accept(CloseEvent closeEvent) {
                            System.err.println("CloseEvent");
                        }
                    }, new Consumer<ErrorEvent>() {
                        @Override
                        public void accept(ErrorEvent errorEvent) {
                            String msg = errorEvent.msg;
                            System.err.println("ErrorEvent msg:" + msg);
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) {
                            Utils.ConsoleExecptionLog(throwable);
                        }
                    })
            );
            WebSocket connect = wsClient.connect();
            int readyState = connect.getReadyState();
            String extensions = connect.getExtensions();
            String protocol = connect.getProtocol();
            String url = connect.getUrl();
            System.err.println(StrUtil.format("readyState:[{}];extensions:[{}];protocol:[{}];url:[{}]",readyState,extensions,protocol,url));
            connect.send("{\"command\":100001}");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
