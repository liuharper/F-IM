package com.yance.fim;

import com.yance.fim.cluster.websocket.WebSocketClientCache;
import com.yance.fim.cluster.websocket.WebsocketClientClusterCreator;
import com.yance.fim.utils.Constants;

import java.util.Timer;
import java.util.TimerTask;

public class WebSocketClientCacheTest {

    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                String uri = "ws://127.0.0.1:9327";
                WebSocketClientCache instance = WebSocketClientCache.getInstance();
                WebsocketClientClusterCreator clientClusterCreator = instance.getKey(uri);
                if (clientClusterCreator == null) {
                    clientClusterCreator = new WebsocketClientClusterCreator(uri, null);
                    instance.putCache(uri, clientClusterCreator);
                    System.err.println("putCache");
                }
                WebsocketClientClusterCreator creator = instance.getKey(uri);
                creator.pushMessage(Constants.HEARTBEAT_PACKET_JSON_STRING);
                System.err.println("pushMessage");
            }
        }, 1000, 3000);
    }
}
