package com.yance.fim;

import cn.hutool.core.util.StrUtil;

public class ThreadNumTest {
    public static void main(String[] args) {
        System.out.println(StrUtil.format("处理器数量为:{}个", Runtime.getRuntime().availableProcessors()));
    }
}
