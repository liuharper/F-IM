/*
 Navicat Premium Data Transfer

 Source Server         : localhost-docker
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3307
 Source Schema         : fim

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 07/09/2020 09:12:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for im_address_books
-- ----------------------------
DROP TABLE IF EXISTS `im_address_books`;
CREATE TABLE `im_address_books` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `type` int(1) DEFAULT NULL COMMENT '类型(1:联系人;2:群聊)',
  `binding_id` bigint(20) DEFAULT NULL COMMENT '绑定ID(当type为1时绑定联系人ID,当type为2时绑定群聊ID)',
  `bind_time` datetime DEFAULT NULL COMMENT '绑定时间',
  `alias` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM用户通讯录';

-- ----------------------------
-- Table structure for im_chatroom
-- ----------------------------
DROP TABLE IF EXISTS `im_chatroom`;
CREATE TABLE `im_chatroom` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `nick_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '群昵称',
  `avatar` text COLLATE utf8mb4_bin COMMENT '群头像',
  `group_account` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '群账号',
  `group_notice` text COLLATE utf8mb4_bin COMMENT '群公告',
  `creator_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `state` int(1) DEFAULT '1' COMMENT '状态(1:正常;2:禁言;3:已解散)',
  `type` int(1) DEFAULT '2' COMMENT '群类型(1:官方群;2:普通群;3:高级群)',
  `verify` int(1) DEFAULT '1' COMMENT '加群时是否需要验证(0:否;1:是)',
  `group_profile` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '群简介',
  `whether_anonymous` int(255) DEFAULT '0' COMMENT '是否开启匿名聊天(0:否;1:是)',
  `whether_pull_historic_records` int(1) DEFAULT '0' COMMENT '是否新用户拉取历史记录(0:否;1:是)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `dissolution_time` datetime DEFAULT NULL COMMENT '解散时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群聊信息表';

-- ----------------------------
-- Table structure for im_conversation_list
-- ----------------------------
DROP TABLE IF EXISTS `im_conversation_list`;
CREATE TABLE `im_conversation_list` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `type` int(1) DEFAULT NULL COMMENT '类型(1:单聊;2:群聊)',
  `binding_id` bigint(20) DEFAULT NULL COMMENT '绑定ID(当type为1时绑定好友ID,当type为2时绑定群聊ID)',
  `conversation_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '会话名称',
  `bind_time` datetime DEFAULT NULL COMMENT '绑定时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM回话列表信息表';

-- ----------------------------
-- Table structure for im_group_messages
-- ----------------------------
DROP TABLE IF EXISTS `im_group_messages`;
CREATE TABLE `im_group_messages` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `m_id` bigint(20) DEFAULT NULL COMMENT '消息ID',
  `sender_id` bigint(20) DEFAULT NULL COMMENT '发送者ID',
  `type` int(1) DEFAULT NULL COMMENT '消息类型(1:文本;2:表情;3:图片;4:文件;5:视频)',
  `data` blob COMMENT '消息内容',
  `create_time` datetime DEFAULT NULL COMMENT '消息创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM消息记录表(群聊)';

-- ----------------------------
-- Table structure for im_group_users
-- ----------------------------
DROP TABLE IF EXISTS `im_group_users`;
CREATE TABLE `im_group_users` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `group_id` bigint(20) DEFAULT NULL COMMENT '群ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `whether_admin` int(1) DEFAULT NULL COMMENT '是否管理员(0:否;1:是;3:群主)',
  `alias` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户群备注',
  `state` int(1) DEFAULT NULL COMMENT '状态(0:正常;1:禁言)',
  `create_time` datetime DEFAULT NULL COMMENT '入群时间',
  `update` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群聊用户信息表';

-- ----------------------------
-- Table structure for im_messages
-- ----------------------------
DROP TABLE IF EXISTS `im_messages`;
CREATE TABLE `im_messages` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `m_id` bigint(20) DEFAULT NULL COMMENT '消息ID',
  `sender_id` bigint(20) DEFAULT NULL COMMENT '发送方ID',
  `receiver_id` bigint(20) DEFAULT NULL COMMENT '接收方ID',
  `type` int(1) DEFAULT NULL COMMENT '消息类型(1:文本;2:表情;3:图片;4:文件;5:视频)',
  `data` blob COMMENT '消息内容',
  `state` int(1) DEFAULT NULL COMMENT '消息状态(0:未读;1:已读)',
  `create_time` datetime DEFAULT NULL COMMENT '消息创建时间',
  `read_time` datetime DEFAULT NULL COMMENT '消息读取时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM消息记录表(单聊)';

-- ----------------------------
-- Table structure for im_user
-- ----------------------------
DROP TABLE IF EXISTS `im_user`;
CREATE TABLE `im_user` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `nick_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户昵称',
  `user_account` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户账号',
  `sex` int(1) DEFAULT '0' COMMENT '用户性别(0:男生;1:女生;默认男生)',
  `age` int(3) DEFAULT '16' COMMENT '用户年龄',
  `cell_phone_number` varchar(16) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户手机号',
  `electronic_mail` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'E-mail(用户邮箱)',
  `avatar` text COLLATE utf8mb4_bin COMMENT '用户头像',
  `type` int(1) DEFAULT '3' COMMENT '用户类型(1:超级管理员;2:系统管理员;3:普通用户)',
  `verify` int(1) DEFAULT '1' COMMENT '加好友时是否需要验证(0:否;1:是)',
  `state` int(1) DEFAULT '1' COMMENT '用户状态(1:正常;2:账号锁定;3:账号异常)',
  `personal_signature` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户个性签名',
  `user_location` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户位置',
  `whether_search` int(1) DEFAULT '1' COMMENT '是否允许被其他用户搜索(0:否;1:是;默认:是)',
  `password_md5` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户密码',
  `create_time` datetime DEFAULT NULL COMMENT '用户注册时间',
  `update_time` datetime DEFAULT NULL COMMENT '用户信息修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM用户信息表';

SET FOREIGN_KEY_CHECKS = 1;
