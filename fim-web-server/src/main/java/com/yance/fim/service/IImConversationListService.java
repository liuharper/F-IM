package com.yance.fim.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yance.fim.entity.ImConversationList;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
public interface IImConversationListService extends IService<ImConversationList> {
	
}
