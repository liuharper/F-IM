package com.yance.fim.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yance.fim.entity.ImAddressBooks;
import com.yance.fim.mapper.ImAddressBooksMapper;
import com.yance.fim.service.IImAddressBooksService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * IM用户通讯录  服务实现类
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
@Service
public class ImAddressBooksServiceImpl extends ServiceImpl<ImAddressBooksMapper, ImAddressBooks> implements IImAddressBooksService {
	
}
