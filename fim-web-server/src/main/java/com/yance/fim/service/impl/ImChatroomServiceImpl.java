package com.yance.fim.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yance.fim.entity.ImChatroom;
import com.yance.fim.mapper.ImChatroomMapper;
import com.yance.fim.service.IImChatroomService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * IM群聊信息表  服务实现类
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
@Service
public class ImChatroomServiceImpl extends ServiceImpl<ImChatroomMapper, ImChatroom> implements IImChatroomService {
	
}
