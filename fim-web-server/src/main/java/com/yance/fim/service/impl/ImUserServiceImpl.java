package com.yance.fim.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.extra.mail.MailUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.yance.fim.dto.ImUserLoginDto;
import com.yance.fim.dto.ImUserRegisterDto;
import com.yance.fim.entity.ImUser;
import com.yance.fim.mapper.ImUserMapper;
import com.yance.fim.service.IImUserService;
import com.yance.fim.utils.ResultVo;
import com.yance.fim.utils.ResultVoUtil;
import com.yance.fim.utils.Utils;
import com.yance.fim.utils.VerificationCodeUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * IM用户信息表  服务实现类
 * </p>
 *
 * @author yance
 * @since 2020-08-17
 */
@Service
public class ImUserServiceImpl extends ServiceImpl<ImUserMapper, ImUser> implements IImUserService {

    @Autowired
    private ImUserMapper imUserMapper;

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Override
    public ResultVo register(ImUserRegisterDto registerDto) {
        try {
            if (!Utils.isEmail(registerDto.getEmail().trim())) {
                return ResultVoUtil.error(1001, "这貌似不是一个邮箱哟");
            }
            IMap<String, String> verificationCodeLists = hazelcastInstance.getMap("VerificationCode");
            String verificationCode = verificationCodeLists.get(registerDto.getEmail());
            if (StringUtils.isNoneBlank(verificationCode)) {
                if (!verificationCode.equals(registerDto.getVerificationCode())) {
                    return ResultVoUtil.error(1002, "验证码错误");
                }else {
                    String remove = verificationCodeLists.remove(registerDto.getEmail());
                }
            } else {
                return ResultVoUtil.error(1003, "验证码已过期");
            }
            ImUser user = new ImUser();
            user.setUserAccount(registerDto.getEmail());
            user.setElectronicMail(registerDto.getEmail());
            user.setPasswordMd5(SecureUtil.md5(registerDto.getPassword()));
            user.setType(3);
            user.setPersonalSignature("该用户还未留下任何足迹");
            user.setCreateTime(new Date());
            imUserMapper.insert(user);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVoUtil.error(1004, "系统繁忙");
        }
        return ResultVoUtil.success(1005, "注册成功");
    }

    @Override
    public ResultVo login(ImUserLoginDto loginDto) {
        try {
            if (!Utils.isEmail(loginDto.getEmail().trim())) {
                return ResultVoUtil.error(1001, "这貌似不是一个邮箱哟");
            }
            ImUser imUser = imUserMapper.findImUserByUserAccount(loginDto.getEmail());
            if (!SecureUtil.md5(loginDto.getPassword()).equals(imUser.getPasswordMd5())) {
                return ResultVoUtil.error(1006, "密码错误");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVoUtil.error(1004, "系统繁忙");
        }
        //这里处理用户session存储
        return ResultVoUtil.success();
    }

    @Override
    public ResultVo<?> sendVerificationCode(String email) {
        try {
            if (!Utils.isEmail(email)) {
                return ResultVoUtil.error(1001, "这貌似不是一个邮箱哟");
            }
            String verificationCode = VerificationCodeUtil.getVerificationCode();
            MailUtil.send(email, "FIM | 注册验证码", StrUtil.format("您的注册验证为:{},有效期为5分钟!", verificationCode), false);
            IMap<String, String> verificationCodeLists = hazelcastInstance.getMap("VerificationCode");
            verificationCodeLists.putIfAbsent(email, verificationCode, 5, TimeUnit.MINUTES);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVoUtil.error(1, "发送失败");
        }
        return ResultVoUtil.success();
    }
}
