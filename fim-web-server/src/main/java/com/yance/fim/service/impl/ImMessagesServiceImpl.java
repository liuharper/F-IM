package com.yance.fim.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yance.fim.entity.ImMessages;
import com.yance.fim.mapper.ImMessagesMapper;
import com.yance.fim.service.IImMessagesService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * IM消息记录表(单聊)  服务实现类
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
@Service
public class ImMessagesServiceImpl extends ServiceImpl<ImMessagesMapper, ImMessages> implements IImMessagesService {
	
}
