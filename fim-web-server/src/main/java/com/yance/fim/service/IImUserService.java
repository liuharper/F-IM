package com.yance.fim.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yance.fim.dto.ImUserLoginDto;
import com.yance.fim.dto.ImUserRegisterDto;
import com.yance.fim.entity.ImUser;
import com.yance.fim.utils.ResultVo;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author yance
 * @since 2020-08-17
 */
public interface IImUserService extends IService<ImUser> {

    ResultVo<?> register(ImUserRegisterDto registerDto);

    ResultVo<?> login(ImUserLoginDto loginDto);

    ResultVo<?> sendVerificationCode(String email);
}
