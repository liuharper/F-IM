package com.yance.fim.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yance.fim.entity.ImGroupMessages;
import com.yance.fim.mapper.ImGroupMessagesMapper;
import com.yance.fim.service.IImGroupMessagesService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * IM消息记录表(群聊)  服务实现类
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
@Service
public class ImGroupMessagesServiceImpl extends ServiceImpl<ImGroupMessagesMapper, ImGroupMessages> implements IImGroupMessagesService {
	
}
