package com.yance.fim.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yance.fim.entity.ImGroupUsers;
import com.yance.fim.mapper.ImGroupUsersMapper;
import com.yance.fim.service.IImGroupUsersService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * IM群聊用户信息表  服务实现类
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
@Service
public class ImGroupUsersServiceImpl extends ServiceImpl<ImGroupUsersMapper, ImGroupUsers> implements IImGroupUsersService {
	
}
