package com.yance.fim.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yance.fim.entity.ImConversationList;
import com.yance.fim.mapper.ImConversationListMapper;
import com.yance.fim.service.IImConversationListService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * IM回话列表信息表  服务实现类
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
@Service
public class ImConversationListServiceImpl extends ServiceImpl<ImConversationListMapper, ImConversationList> implements IImConversationListService {
	
}
