package com.yance.fim.controller;

import com.yance.fim.dto.ImUserLoginDto;
import com.yance.fim.dto.ImUserRegisterDto;
import com.yance.fim.service.IImUserService;
import com.yance.fim.utils.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value = "imUser", tags = "IM用户接口")
@RequestMapping("/im")
public class ImUserController {

    @Autowired
    private IImUserService iImUserService;

    @PostMapping(path = "/register", produces = "application/json;charset=UTF-8")
    @ApiOperation(value = "IM用户注册接口")
    public ResultVo<?> register(@RequestBody ImUserRegisterDto registerDto) {
        return iImUserService.register(registerDto);
    }

    @PostMapping(path = "/login", produces = "application/json;charset=UTF-8")
    @ApiOperation(value = "IM用户登录接口")
    public ResultVo<?> login(@RequestBody ImUserLoginDto loginDto) {
        return iImUserService.login(loginDto);
    }

    @GetMapping(path = "/sendVerificationCode", produces = "application/json;charset=UTF-8")
    @ApiOperation(value = "IM用户获取注册验证码")
    public ResultVo<?> sendVerificationCode(@RequestParam(name = "email") String email) {
        return iImUserService.sendVerificationCode(email);
    }
}
