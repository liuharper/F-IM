package com.yance.fim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * IM用户信息表
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
@TableName("im_user")
public class ImUser implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 用户昵称
	 */
	@TableField(value="nick_name")
	private String nickName;

	/**
	 * 用户账号
	 */
	@TableField(value="user_account")
	private String userAccount;

	/**
	 * 用户性别(0:男生;1:女生;默认男生)
	 */
	private Integer sex;

	/**
	 * 用户年龄
	 */
	private Integer age;

	/**
	 * 用户手机号
	 */
	@TableField(value="cell_phone_number")
	private String cellPhoneNumber;

	/**
	 * E-mail(用户邮箱)
	 */
	@TableField(value="electronic_mail")
	private String electronicMail;

	/**
	 * 用户头像
	 */
	private String avatar;

	/**
	 * 用户类型(1:超级管理员;2:系统管理员;3:普通用户)
	 */
	private Integer type;

	/**
	 * 加好友时是否需要验证(0:否;1:是)
	 */
	private Integer verify;

	/**
	 * 用户状态(1:正常;2:账号锁定;3:账号异常)
	 */
	private Integer state;

	/**
	 * 用户个性签名
	 */
	@TableField(value="personal_signature")
	private String personalSignature;

	/**
	 * 用户位置
	 */
	@TableField(value="user_location")
	private String userLocation;

	/**
	 * 是否允许被其他用户搜索(0:否;1:是;默认:是)
	 */
	@TableField(value="whether_search")
	private Integer whetherSearch;

	/**
	 * 用户密码
	 */
	@TableField(value="password_md5")
	private String passwordMd5;

	/**
	 * 用户注册时间
	 */
	@TableField(value="create_time")
	private Date createTime;

	/**
	 * 用户信息修改时间
	 */
	@TableField(value="update_time")
	private Date updateTime;



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getCellPhoneNumber() {
		return cellPhoneNumber;
	}

	public void setCellPhoneNumber(String cellPhoneNumber) {
		this.cellPhoneNumber = cellPhoneNumber;
	}

	public String getElectronicMail() {
		return electronicMail;
	}

	public void setElectronicMail(String electronicMail) {
		this.electronicMail = electronicMail;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getVerify() {
		return verify;
	}

	public void setVerify(Integer verify) {
		this.verify = verify;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getPersonalSignature() {
		return personalSignature;
	}

	public void setPersonalSignature(String personalSignature) {
		this.personalSignature = personalSignature;
	}

	public String getUserLocation() {
		return userLocation;
	}

	public void setUserLocation(String userLocation) {
		this.userLocation = userLocation;
	}

	public Integer getWhetherSearch() {
		return whetherSearch;
	}

	public void setWhetherSearch(Integer whetherSearch) {
		this.whetherSearch = whetherSearch;
	}

	public String getPasswordMd5() {
		return passwordMd5;
	}

	public void setPasswordMd5(String passwordMd5) {
		this.passwordMd5 = passwordMd5;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
