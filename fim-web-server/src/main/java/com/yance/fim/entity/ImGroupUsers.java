package com.yance.fim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * IM群聊用户信息表
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
@TableName("im_group_users")
public class ImGroupUsers implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 群ID
	 */
	@TableField(value="group_id")
	private Long groupId;

	/**
	 * 用户ID
	 */
	@TableField(value="user_id")
	private Long userId;

	/**
	 * 是否管理员(0:否;1:是;3:群主)
	 */
	@TableField(value="whether_admin")
	private Integer whetherAdmin;

	/**
	 * 用户群备注
	 */
	private String alias;

	/**
	 * 状态(0:正常;1:禁言)
	 */
	private Integer state;

	/**
	 * 入群时间
	 */
	@TableField(value="create_time")
	private Date createTime;

	/**
	 * 修改时间
	 */
	private Date update;



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getWhetherAdmin() {
		return whetherAdmin;
	}

	public void setWhetherAdmin(Integer whetherAdmin) {
		this.whetherAdmin = whetherAdmin;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdate() {
		return update;
	}

	public void setUpdate(Date update) {
		this.update = update;
	}

}
