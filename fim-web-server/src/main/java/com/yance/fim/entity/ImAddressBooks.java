package com.yance.fim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * IM用户通讯录
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
@TableName("im_address_books")
public class ImAddressBooks implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 用户ID
	 */
	@TableField(value="user_id")
	private Long userId;

	/**
	 * 类型(1:联系人;2:群聊)
	 */
	private Integer type;

	/**
	 * 绑定ID(当type为1时绑定联系人ID,当type为2时绑定群聊ID)
	 */
	@TableField(value="binding_id")
	private Long bindingId;

	/**
	 * 绑定时间
	 */
	@TableField(value="bind_time")
	private Date bindTime;

	/**
	 * 备注
	 */
	private String alias;



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getBindingId() {
		return bindingId;
	}

	public void setBindingId(Long bindingId) {
		this.bindingId = bindingId;
	}

	public Date getBindTime() {
		return bindTime;
	}

	public void setBindTime(Date bindTime) {
		this.bindTime = bindTime;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

}
