package com.yance.fim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * IM群聊信息表
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
@TableName("im_chatroom")
public class ImChatroom implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 群昵称
	 */
	@TableField(value="nick_name")
	private String nickName;

	/**
	 * 群头像
	 */
	private String avatar;

	/**
	 * 群账号
	 */
	@TableField(value="group_account")
	private String groupAccount;

	/**
	 * 群公告
	 */
	@TableField(value="group_notice")
	private String groupNotice;

	/**
	 * 创建者ID
	 */
	@TableField(value="creator_id")
	private Long creatorId;

	/**
	 * 状态(1:正常;2:禁言;3:已解散)
	 */
	private Integer state;

	/**
	 * 群类型(1:官方群;2:普通群;3:高级群)
	 */
	private Integer type;

	/**
	 * 加群时是否需要验证(0:否;1:是)
	 */
	private Integer verify;

	/**
	 * 群简介
	 */
	@TableField(value="group_profile")
	private String groupProfile;

	/**
	 * 是否开启匿名聊天(0:否;1:是)
	 */
	@TableField(value="whether_anonymous")
	private Integer whetherAnonymous;

	/**
	 * 是否新用户拉取历史记录(0:否;1:是)
	 */
	@TableField(value="whether_pull_historic_records")
	private Integer whetherPullHistoricRecords;

	/**
	 * 创建时间
	 */
	@TableField(value="create_time")
	private Date createTime;

	/**
	 * 修改时间
	 */
	@TableField(value="update_time")
	private Date updateTime;

	/**
	 * 解散时间
	 */
	@TableField(value="dissolution_time")
	private Date dissolutionTime;



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getGroupAccount() {
		return groupAccount;
	}

	public void setGroupAccount(String groupAccount) {
		this.groupAccount = groupAccount;
	}

	public String getGroupNotice() {
		return groupNotice;
	}

	public void setGroupNotice(String groupNotice) {
		this.groupNotice = groupNotice;
	}

	public Long getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getVerify() {
		return verify;
	}

	public void setVerify(Integer verify) {
		this.verify = verify;
	}

	public String getGroupProfile() {
		return groupProfile;
	}

	public void setGroupProfile(String groupProfile) {
		this.groupProfile = groupProfile;
	}

	public Integer getWhetherAnonymous() {
		return whetherAnonymous;
	}

	public void setWhetherAnonymous(Integer whetherAnonymous) {
		this.whetherAnonymous = whetherAnonymous;
	}

	public Integer getWhetherPullHistoricRecords() {
		return whetherPullHistoricRecords;
	}

	public void setWhetherPullHistoricRecords(Integer whetherPullHistoricRecords) {
		this.whetherPullHistoricRecords = whetherPullHistoricRecords;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getDissolutionTime() {
		return dissolutionTime;
	}

	public void setDissolutionTime(Date dissolutionTime) {
		this.dissolutionTime = dissolutionTime;
	}

}
