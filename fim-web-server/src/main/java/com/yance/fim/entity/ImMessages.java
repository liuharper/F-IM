package com.yance.fim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * IM消息记录表(单聊)
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
@TableName("im_messages")
public class ImMessages implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 消息ID
	 */
	@TableField(value="m_id")
	private Long mId;

	/**
	 * 发送方ID
	 */
	@TableField(value="sender_id")
	private Long senderId;

	/**
	 * 接收方ID
	 */
	@TableField(value="receiver_id")
	private Long receiverId;

	/**
	 * 消息类型(1:文本;2:表情;3:图片;4:文件;5:视频)
	 */
	private Integer type;

	/**
	 * 消息内容
	 */
	private byte[] data;

	/**
	 * 消息状态(0:未读;1:已读)
	 */
	private Integer state;

	/**
	 * 消息创建时间
	 */
	@TableField(value="create_time")
	private Date createTime;

	/**
	 * 消息读取时间
	 */
	@TableField(value="read_time")
	private Date readTime;



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMId() {
		return mId;
	}

	public void setMId(Long mId) {
		this.mId = mId;
	}

	public Long getSenderId() {
		return senderId;
	}

	public void setSenderId(Long senderId) {
		this.senderId = senderId;
	}

	public Long getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(Long receiverId) {
		this.receiverId = receiverId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getReadTime() {
		return readTime;
	}

	public void setReadTime(Date readTime) {
		this.readTime = readTime;
	}

}
