package com.yance.fim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * IM回话列表信息表
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
@TableName("im_conversation_list")
public class ImConversationList implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 用户ID
	 */
	@TableField(value="user_id")
	private Long userId;

	/**
	 * 类型(1:单聊;2:群聊)
	 */
	private Integer type;

	/**
	 * 绑定ID(当type为1时绑定好友ID,当type为2时绑定群聊ID)
	 */
	@TableField(value="binding_id")
	private Long bindingId;

	/**
	 * 会话名称
	 */
	@TableField(value="conversation_name")
	private String conversationName;

	/**
	 * 绑定时间
	 */
	@TableField(value="bind_time")
	private Date bindTime;



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getBindingId() {
		return bindingId;
	}

	public void setBindingId(Long bindingId) {
		this.bindingId = bindingId;
	}

	public String getConversationName() {
		return conversationName;
	}

	public void setConversationName(String conversationName) {
		this.conversationName = conversationName;
	}

	public Date getBindTime() {
		return bindTime;
	}

	public void setBindTime(Date bindTime) {
		this.bindTime = bindTime;
	}

}
