package com.yance.fim.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * IM消息记录表(群聊)
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
@TableName("im_group_messages")
public class ImGroupMessages implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 消息ID
	 */
	@TableField(value="m_id")
	private Long mId;

	/**
	 * 发送者ID
	 */
	@TableField(value="sender_id")
	private Long senderId;

	/**
	 * 消息类型(1:文本;2:表情;3:图片;4:文件;5:视频)
	 */
	private Integer type;

	/**
	 * 消息内容
	 */
	private byte[] data;

	/**
	 * 消息创建时间
	 */
	@TableField(value="create_time")
	private Date createTime;



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMId() {
		return mId;
	}

	public void setMId(Long mId) {
		this.mId = mId;
	}

	public Long getSenderId() {
		return senderId;
	}

	public void setSenderId(Long senderId) {
		this.senderId = senderId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
