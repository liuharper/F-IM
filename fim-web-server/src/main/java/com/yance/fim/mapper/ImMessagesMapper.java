package com.yance.fim.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yance.fim.entity.ImMessages;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author yance
 * @since 2020-08-25
 */
@Mapper
public interface ImMessagesMapper extends BaseMapper<ImMessages> {

}