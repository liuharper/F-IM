package com.yance.fim.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yance.fim.entity.ImUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author yance
 * @since 2020-08-17
 */
@Mapper
public interface ImUserMapper extends BaseMapper<ImUser> {

    ImUser findImUserByUserAccount(@Param("userAccount") String userAccount);
}