package com.yance.fim;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author yance
 */
@SpringBootApplication
@MapperScan("com.yance.fim.mapper")
public class FimWebServerApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(FimWebServerApplication.class);

    public static void main(String[] args) throws UnknownHostException {
        //指定项目中使用的日志框架
        System.setProperty("dubbo.application.logger", "log4j2");
        ConfigurableApplicationContext run = SpringApplication.run(FimWebServerApplication.class, args);
        Environment env = run.getEnvironment();
        LOGGER.info("\n[----------------------------------------------------------]\n\t" +
                        "启动成功！点击进入:\thttp://{}:{}" +
                        "\n[----------------------------------------------------------",
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port") + "/swagger-ui.html");
    }
}
